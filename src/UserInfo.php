<?php

namespace Dottystyle\LaravelSSO;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use BadMethodCallException;
use ArrayAccess;

class UserInfo implements Arrayable, ArrayAccess
{
    /**
     * @var array
     */
    protected $info;

    /**
     * @param array $info
     */
    public function __construct(array $info) 
    {
        $this->info = $info;
    }

    /**
     * Get the id of the user.
     * 
     * @return mixed
     */
    public function getId()
    {
        return $this->info['id'];
    }

    /**
     * Get the attribute value of the user info given the attribute name.
     * 
     * @param string $key
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->info[$name];
    }

    /**
     * 
     * @param string $name
     * @return bool
     */
    public function __isset(string $name)
    {
        return isset($this->info[$name]);
    }

    /**
     * Convert the instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->info;
    }

    /**
     * Whether an offset exists or not
     * 
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->info[$offset]);
    }

    /**
     * Offset to retrieve
     * 
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->info[$offset];
    }

    /**
     * Assign a value to the specified offset
     * 
     * @param mixed $offset
     * @param mixed $value
     * @return void
     * 
     * @throws \BadMethodCallException
     */
    public function offsetSet($offset, $value)
    {
        throw new BadMethodCallException("Setting $offset of SSO user info is not allowed");
    }

    /**
     * Unset an offset
     * 
     * @param mixed $offset
     * @return void
     * 
     * @throws \BadMethodCallException
     */
    public function offsetUnset($offset)
    {
        throw new BadMethodCallException("Unsetting $offset of SSO user info is not allowed");
    }
}