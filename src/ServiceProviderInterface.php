<?php

namespace Dottystyle\LaravelSSO;

interface ServiceProviderInterface
{
    /**
     * Get the id of service provider.
     * 
     * @return mixed
     */
    public function getId();

    /**
     * Get the name of service provider.
     * 
     * @return string
     */
    public function getName();

    /**
     * Get the secret key of service provider.
     * 
     * @return string
     */
    public function getSecret();
}