<?php

namespace Dottystyle\LaravelSSO;

interface TokenInterface
{
    /**
     * Get the token identifier.
     * 
     * @return string
     */
    public function getTokenId();

    /**
     * Get the user id from token.
     * 
     * @return mixed
     */
    public function getUserId();

    /**
     * Determine whether the token is expired or not.
     * 
     * @return boolean
     */
    public function expired();

    /**
     * Get the remaining minutes of the token.
     * 
     * @return int
     */
    public function remainingMinutes();
}