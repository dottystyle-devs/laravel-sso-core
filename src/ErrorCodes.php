<?php

namespace Dottystyle\LaravelSSO;

final class ErrorCodes
{
    const TOKEN_MISMATCH = 'TOKEN_MISMATCH';
    const UNAUTHENTICATED = 'UNAUTHENTICATED';
    const FATAL_ERROR = 'FATAL_ERROR';
}