<?php

namespace Dottystyle\LaravelSSO\Exceptions;

class LoginException extends AuthenticationException
{
    /**
     * Create new instance of the exception.
     */
    public function __construct($message = '')
    {
        parent::__construct($message ?: 'Login failed');
    }
}