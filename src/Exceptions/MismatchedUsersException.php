<?php

namespace Dottystyle\LaravelSSO\Exceptions;

class MismatchedUsersException extends AuthenticationException
{
}