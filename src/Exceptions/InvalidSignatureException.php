<?php

namespace Dottystyle\LaravelSSO\Exceptions;

class InvalidSignatureException extends SSOException
{
}