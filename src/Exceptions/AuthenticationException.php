<?php

namespace Dottystyle\LaravelSSO\Exceptions;

use Dottystyle\LaravelSSO\ErrorCodes;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class AuthenticationException extends SSOException implements HttpExceptionInterface
{
    /**
     * @var string
     */
    protected $errorCode = ErrorCodes::UNAUTHENTICATED;

    /**
     * Returns the status code.
     *
     * @return int An HTTP response status code
     */
    public function getStatusCode()
    {
        return 401;
    }

    /**
     * Returns response headers.
     *
     * @return array Response headers
     */
    public function getHeaders()
    {
        return [];
    }
}