<?php

namespace Dottystyle\LaravelSSO\Exceptions;

use Dottystyle\LaravelSSO\ErrorCodes;
use Exception;

class SSOException extends Exception implements SSOExceptionInterface
{
    /**
     * @var string
     */
    protected $errorCode;
 
    /**
     * Create new exception instance.
     * 
     * @param string $message
     * @param mixed $errorCode
     */
    public function __construct($message = '', $errorCode = ErrorCodes::FATAL_ERROR)
    {
        parent::__construct($message);

        $this->errorCode = $errorCode;
    }

    /**
     * Get the custom error code of the SSO exception.
     * 
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}