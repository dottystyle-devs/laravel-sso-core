<?php

namespace Dottystyle\LaravelSSO\Exceptions;

interface SSOExceptionInterface
{
    /**
     * Get the custom error code of the exception.
     * 
     * @return mixed
     */
    public function getErrorCode();
}