<?php

namespace Dottystyle\LaravelSSO\Exceptions;

class MissingTokenException extends SSOException
{
}