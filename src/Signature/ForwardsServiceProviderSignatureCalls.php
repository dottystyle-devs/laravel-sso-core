<?php

namespace Dottystyle\LaravelSSO\Signature;

trait ForwardsServiceProviderSignatureCalls
{
    /**
     * Forward some calls to the signature assistant.
     * 
     * @param \Dottystyle\LaravelSSO\Signature\ServiceProviderSignatureAssistant $sig
     * @param string $method
     * @param array $arguments
     * @param ref $matched
     * @return mixed
     */
    private function forwardToSignature(ServiceProviderSignatureAssistant $sig, $method, array $arguments, &$matched = false)
    {   
        $matched = false;

        if ((strpos($method, 'verify') === 0 || strpos($method, 'sign') === 0) && method_exists($sig, $method)) {
            $matched = true;

            return $sig->{$method}(...$arguments);
        }
    }
}