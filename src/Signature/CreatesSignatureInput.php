<?php

namespace Dottystyle\LaravelSSO\Signature;

use Dottystyle\LaravelSSO\ServiceProviderInterface;

trait CreatesSignatureInput
{
    /**
     * Create signature input for login request.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $nonce
     * @param string $extra (optional)
     * @return string
     */
    protected function loginSignatureInput(ServiceProviderInterface $sp, $nonce, $extra = '')
    {
        return 'login:'.$sp->getId().$nonce.$extra;
    }

    /**
     * Create signature input for a successful login.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @param string $nonce
     * @return string
     */
    protected function loginSuccessSignatureInput(ServiceProviderInterface $sp, $token, $userId, $nonce)
    {
        return 'login_success:'.$sp->getId().$token.$userId.$nonce;
    }

    /**
     * Create signature input for a failed login.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $nonce 
     * @return string
     */
    protected function loginFailSignatureInput(ServiceProviderInterface $sp, $nonce)
    {
        return 'login_fail:'.$sp->getId().$nonce;
    }

    /**
     * Create signature input for fetching token stat.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string salt
     * @return string
     */
    protected function tokenStatSignatureInput(ServiceProviderInterface $sp, $salt)
    {
        return 'token_stat:'.$sp->getId().$salt;
    }

    /**
     * Create signature input for successful token stat.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param string salt
     * @return string
     */
    protected function tokenStatSuccessSignatureInput(ServiceProviderInterface $sp, $token, $salt)
    {
        return 'token_stat_success:'.$sp->getId().$token.$salt;
    }

    /**
     * Create signature input for successful token stat.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param string salt
     * @return string
     */
    protected function tokenCompareSignatureInput(ServiceProviderInterface $sp, $token, $salt)
    {
        return 'token_compare:'.$sp->getId().$token.$salt;
    }

    /**
     * Create signature input for logout request.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    protected function logoutSignatureInput(ServiceProviderInterface $sp, $token, $userId)
    {
        return 'logout:'.$sp->getId().$token.$userId;
    }

   /**
     * Create signature input for successful logout response.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    protected function logoutSuccessSignatureInput(ServiceProviderInterface $sp, $token, $userId)
    {
        return 'logout_success:'.$sp->getId().$token.$userId;
    }
}