<?php

namespace Dottystyle\LaravelSSO\Signature;

use Dottystyle\LaravelSSO\ServiceProviderInterface;

final class Checksum
{
    /**
     * Make the input string for generating service provider login request checksum.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $nonce
     * @return string
     */
    public static function makeLoginRequestInput(ServiceProviderInterface $sp, $nonce, $extra = '')
    {
        return 'login_request:'.$sp->getId().$nonce.$extra;
    }

    /**
     * Make the input string for successful login response checksum.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @param string $extra (optional)
     * @return string
     */
    public static function makeLoginSuccessInput(ServiceProviderInterface $sp, $token, $userId, $extra = '')
    {
        return 'login_receive:'.$sp->getId().$token.$userId.$extra;
    }

    /**
     * Make the input string for failed login checksum.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @return string
     */
    public static function makeLoginFailedInput(ServiceProviderInterface $sp) 
    {
        return 'login_failed:'.$sp->getId();
    }

    /**
     * Make the input string for fetching the user info.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string salt
     * @return string
     */
    public static function makeGetUserInfoInput(ServiceProviderInterface $sp, $salt)
    {
        return 'user:'.$sp->getId().$salt;
    }

    /**
     * Make the input string for logout request.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public static function makeLogoutRequestInput(ServiceProviderInterface $sp, $token, $userId)
    {
        return 'logout:'.$sp->getId().$token.$userId;
    }

    /**
     * Make the input string for logout request.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public static function makeLogoutResponseInput(ServiceProviderInterface $sp, $token, $userId)
    {
        return 'logout_success:'.$sp->getId().$token.$userId;
    }
}