<?php

namespace Dottystyle\LaravelSSO\Signature;

use Dottystyle\LaravelSSO\ServiceProviderInterface;
use Dottystyle\LaravelSSO\Signature\SignatureAssistant;
use Dottystyle\LaravelSSO\Signature\Concerns\CreatesAndVerifiesSignature;
use BadMethodCallException;

class SignatureProxy
{
    use Concerns\CreatesAndVerifiesSignature, 
        Concerns\ForwardsCalls;

    /**
     * @var \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    protected $serviceProvider;

    /**
     * Create new instance of the proxy.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param \Dottystyle\LaravelSSO\Signature\SignatureAssistant $sig
     */
    public function __construct(ServiceProviderInterface $sp, SignatureAssistant $sig)
    {
        $this->serviceProvider = $sp;
        $this->setSignatureAssistant($sig);
    }

    /**
     * Forward some of the calls to the signature assistant.
     * 
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, array $arguments)
    {
        return $this->forwardSignatureCall($this->$serviceProvider, $method, $arguments);
    }
}