<?php

namespace Dottystyle\LaravelSSO\Signature;

interface SignatureAssistant 
{
    /**
     * Hash the given value.
     *
     * @param string $value
     * @param string $secret
     * @param array $options (optional)
     * @return string
     */
    public function make($value, $secret, array $options = []);

    /**
     * Verify the given plain value against a hash.
     *
     * @param string $value
     * @param string $hashedValue
     * @param string $secret
     * @param array $options (optional)
     * @return bool
     */
    public function verify($value, $hashedValue, $secret, array $options = []);    
}