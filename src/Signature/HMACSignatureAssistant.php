<?php

namespace Dottystyle\LaravelSSO\Signature;

use hash_hmac;

class HMACSignatureAssistant implements SignatureAssistant
{
    /**
     * Hash the given value.
     *
     * @param string  $value
     * @param string $secret
     * @param array $options (optional)
     * @return string
     */
    public function make($value, $secret, array $options = [])
    {
        return $this->encode($this->makeRaw($value, $secret, $this->algo($options)));
    }

    /**
     * Generate the hash value.
     * 
     * @param string $value
     * @param string $secret
     * @param string $algo
     * @return mixed
     */
    protected function makeRaw($value, $secret, $algo)
    {
        return hash_hmac($algo, $value, $secret, true);
    }

    /**
     * Verify the given plain value against a hash.
     *
     * @param string $value
     * @param string $userValue
     * @param string $secret
     * @param array $options (optional)
     * @return bool
     */
    public function verify($value, $userValue, $secret, array $options = [])
    {
        return hash_equals($this->makeRaw($value, $secret, $this->algo($options)), $this->decode($userValue));
    }

    /**
     * Encode the given string.
     * 
     * @return string
     */
    protected function encode($value)
    {
        return base64_encode($value);
    }

    /**
     * Decode the given string.
     * 
     * @return string
     */
    protected function decode($value)
    {
        return base64_decode($value);
    }

    /**
     * Get the algorithm for generating the hash from the options.
     * 
     * @param array $options
     * @return string
     */
    protected function algo(array $options)
    {
        return $options['algo'] ?? 'sha256';
    }
}