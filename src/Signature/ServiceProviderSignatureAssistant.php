<?php

namespace Dottystyle\LaravelSSO\Signature;

use Dottystyle\LaravelSSO\ServiceProviderInterface;
use Dottystyle\LaravelSSO\Exceptions\InvalidSignatureException;

class ServiceProviderSignatureAssistant 
{
    use CreatesSignatureInput;

    /**
     * @var \Dottystyle\LaravelSSO\Signature\SignatureAssistant
     */
    protected $sig;

    /**
     * @var array
     */
    protected $makeOptions = [];
    
    /**
     * @var array
     */
    protected $verifyOptions = [];

    /**
     * Create new instance of the signature assistant.
     * 
     * @param \Dottystyle\LaravelSSO\Signature\SignatureAssistant
     * @param array $makeOptions
     * @param array $verifyOptions
     */
    public function __construct(SignatureAssistant $sig, array $makeOptions = [], array $verifyOptions = [])
    {
        $this->sig = $sig;
        $this->makeOptions = $makeOptions;
        $this->verifyOptions = $verifyOptions;
    }

    /**
     * Set the service provider to create and verify signatures.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface
     * @return static
     */
    public function setServiceProvider(ServiceProviderInterface $sp)
    {
        $this->serviceProvider = $sp;

        return $this;
    }

    /**
     * Get the service provider.
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function getServiceProvider()
    {
        return $this->serviceProvider;
    }

    /**
     * Generate signature.
     * 
     * @param mixed $data
     * @return string
     */
    public function sign($data)
    {
        return $this->sig->make($data, $this->serviceProvider->getSecret(), $this->makeOptions);
    }

    /**
     * Verify that the given signature is valid.
     * 
     * @param string $expected
     * @param string $value
     * @return void
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\InvalidSignatureException
     */
    public function verify($expected, $value)
    {
        if (! $this->sig->verify($expected, $value, $this->serviceProvider->getSecret(), $this->verifyOptions)) {
            throw new InvalidSignatureException();
        }
    }

    /**
     * Create signature for login request.
     * 
     * @param string $nonce
     * @param string $extra (optional)
     * @return string
     */
    public function signLogin($nonce, $extra = '')
    {
        return $this->sign($this->loginSignatureInput($this->serviceProvider, $nonce, $extra));
    }

    /**
     * Create signature for a successful login.
     * 
     * @param string $token
     * @param mixed $userId
     * @param string $nonce
     * @return string
     */
    public function signLoginSuccess($token, $userId, $nonce)
    {
        return $this->sign($this->loginSuccessSignatureInput($this->serviceProvider, $token, $userId, $nonce));
    }

    /**
     * Create signature for a failed login.
     * 
     * @param mixed $userId
     * @param string $extra (optional)
     * @return string
     */
    public function signLoginFail($extra = '')
    {
        return $this->sign($this->loginFailSignatureInput($this->serviceProvider, $extra));
    }

    /**
     * Create signature for fetching token stat.
     * 
     * @param string salt
     * @return string
     */
    public function signTokenStat($salt)
    {
        return $this->sign($this->tokenStatSignatureInput($this->serviceProvider, $salt));
    }

    /**
     * Create signature for successful token stat.
     * 
     * @param string $token
     * @param string $salt
     * @return string
     */
    public function signTokenStatSuccess($token, $salt)
    {
        return $this->sign($this->tokenStatSuccessSignatureInput($this->serviceProvider, $token, $salt));
    }

    /**
     * Create signature for token comparison request.
     * 
     * @param string $token
     * @param string $salt
     * @return string
     */
    public function signTokenCompare($token, $salt)
    {
        return $this->sign($this->tokenCompareSignatureInput($this->serviceProvider, $token, $salt));
    }

    /**
     * Create signature for logout request.
     * 
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public function signLogout($token, $userId)
    {
        return $this->sign($this->logoutSignatureInput($this->serviceProvider, $token, $userId));
    }

    /**
     * Create signature for successful logout.
     * 
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public function signLogoutSuccess($token, $userId)
    {
        return $this->sign($this->logoutSuccessSignatureInput($this->serviceProvider, $token, $userId));
    }

    /**
     * Verify the signature for login request.
     * 
     * @param string $signature
     * @param string $nonce
     * @param string $extra (optional)
     * @return mixed
     */
    public function verifyLogin($signature, $nonce, $extra = '')
    {
        return $this->verify($this->loginSignatureInput($this->serviceProvider, $nonce, $extra), $signature);
    }

    /**
     * Verify signature for a successful login.
     * 
     * @param string $signature
     * @param string $token
     * @param mixed $userId
     * @param string $nonce
     * @return mixed
     */
    public function verifyLoginSuccess($signature, $token, $userId, $nonce)
    {
        return $this->verify($this->loginSuccessSignatureInput($this->serviceProvider, $token, $userId, $nonce), $signature);
    }

    /**
     * Verify signature for a failed login.
     * 
     * @param string $signature
     * @param mixed $nonce
     * @return mixed
     */
    public function verifyLoginFail($signature, $nonce)
    {
        return $this->verify($this->loginFailSignatureInput($this->serviceProvider, $nonce), $signature);
    }

    /**
     * Verify signature for fetching token stats.
     * 
     * @param string $signature
     * @param string salt
     * @return mixed
     */
    public function verifyTokenStat($signature, $salt)
    {
        return $this->verify($this->tokenStatSignatureInput($this->serviceProvider, $salt), $signature);
    }

    /**
     * Verify signature for a successful token stat.
     * 
     * @param string $signature
     * @param string $token
     * @param string salt
     * @return mixed
     */
    public function verifyTokenStatSuccess($signature, $token, $salt)
    {
        return $this->verify($this->tokenStatSuccessSignatureInput($this->serviceProvider, $token, $salt), $signature);
    }

    /**
     * Verify signature of token comparison request.
     * 
     * @param string $signature
     * @param string $token
     * @param string salt
     * @return mixed
     */
    public function verifyTokenCompare($signature, $token, $salt)
    {
        return $this->verify($this->tokenCompareSignatureInput($this->serviceProvider, $token, $salt), $signature);
    }

    /**
     * Verify signature for logout request.
     * 
     * @param string $signature
     * @param string $token
     * @param mixed $userId
     * @return mixed
     */
    public function verifyLogout($signature, $token, $userId)
    {
        return $this->verify($this->logoutSignatureInput($this->serviceProvider, $token, $userId), $signature);
    }

    /**
     * Verify signature input for successful logout.
     * 
     * @param string $signature
     * @param string $token
     * @param mixed $userId
     * @return mixed
     */
    public function verifyLogoutSuccess($signature, $token, $userId)
    {
        return $this->verify($this->logoutSuccessSignatureInput($this->serviceProvider, $token, $userId), $signature);
    }
}