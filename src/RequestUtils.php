<?php

namespace Dottystyle\LaravelSSO;

use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Str;

final class RequestUtils
{
    /**
     * Determine whether the request accepts image as response via Accept header.
     * 
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    public static function acceptsImage(HttpRequest $request)
    {
        $type = $request->getAcceptableContentTypes()[0] ?? '';

        return Str::contains($type, 'image/');
    }

    /**
     * Create a response with transparent 1x1 PNG image payload.
     * 
     * @return \Illuminate\Http\Response
     */
    public static function transparent1x1PngResponse()
    {
        // Do not cache the result and set a cookie to check whether we have logged in or not, good for only a minute.
        return response(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII='))
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
            ->header('Content-Type', 'image/png');
    }

    /**
     * Append the given parameters to the specified URL.
     * 
     * @param string $path
     * @return string
     */
    public static function appendUrlParams($url, array $params)
    {
        return $url.(parse_url($url, PHP_URL_QUERY) ? '&' : '?').http_build_query($params);
    }

    /**
     * Create a random string for salt.
     * 
     * @return string
     */
    public static function salt()
    {
        return Str::random(10);
    }
}